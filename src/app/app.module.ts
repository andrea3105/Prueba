import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// importacion oauth
import { OAuthModule } from 'angular-oauth2-oidc';
// importacion social auth
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angular5-social-auth';
// importacion para el servicio
import { HttpClientModule } from '@angular/common/http';
import { ServicioService } from './servicio.service';
// importacion para usar la api de google
import { GoogleApiModule, GoogleApiService, GoogleAuthService, NgGapiClientConfig,
NG_GAPI_CONFIG, GoogleApiConfig } from 'ng-gapi';

// importaciones de material
import 'hammerjs';
import { MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule, MatToolbarModule,
  MatListModule } from '@angular/material';
// rutas
import { APP_ROUTING } from './app.routes';

// componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SideComponent } from './dashboard/side/side.component';
import { MuestraComponent } from './dashboard/side/muestra/muestra.component';


// guarda el id de client obtenido en Google OAuth
export function getAuthServiceConfigs() {
  const config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('52304866396-lt1td1fapk8va8s9t2jipf9lusloig0v.apps.googleusercontent.com')
      }
    ]);
    return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    SideComponent,
    MuestraComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    OAuthModule.forRoot(),
    SocialLoginModule,
    HttpClientModule,
    GoogleApiModule,
  ],
  providers: [
  ServicioService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }],
  bootstrap: [AppComponent]
})


export class AppModule { }


