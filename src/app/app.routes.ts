import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SideComponent } from './dashboard/side/side.component';
import { MuestraComponent } from './dashboard/side/muestra/muestra.component';


const APP_ROUTES: Routes = [
  { path: '', component:  LoginComponent},
  { path: 'inicio', component: DashboardComponent },
  { path: 'muestra', component:  MuestraComponent},
  { path: '**', pathMatch: 'full', redirectTo: ''}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
