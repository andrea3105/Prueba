import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../../../servicio.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-muestra',
  templateUrl: './muestra.component.html',
  styleUrls: ['./muestra.component.css']
})
export class MuestraComponent implements OnInit {

  constructor(private servi: ServicioService, public resp: HttpClient) { }
  obj: any;
  Servicio(): void {

    this.servi.getServ().subscribe(
    resp => {
      console.log(resp);
      this.obj = resp;

    }, error => {
      console.log(<any> error);
    }
    );
    }

  ngOnInit() {
  }

}
