import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side',
  templateUrl: './side.component.html',
  styleUrls: ['./side.component.css']
})
export class SideComponent implements OnInit {
  mostrar: boolean;
// convirte la data guardada en local storage a json para poder ser llamada desde e html
  username = JSON.parse(localStorage.getItem('obje'));
  constructor() {
  }

  ngOnInit() {
  }



  irmuestra() {
    this.mostrar = false;
  }

}
