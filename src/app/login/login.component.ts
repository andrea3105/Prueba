import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider } from 'angular5-social-auth';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private socialAuthService: AuthService) { }
// metodo que indica que la autenticacion es por google y lo asocia con el ID-CLIENT

   public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
// Muestra por consola los datos obtenidos del usuario
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data: ', userData);
        // se guarda la data obtenida en localstorage convirtiendo el json a string
        localStorage.setItem('obje', JSON.stringify(userData));
        // y se redirige a la pantalla de inicio
        this.router.navigate(['inicio']);

      }
    );
  }

  ngOnInit() {
  }

  loguear() {
    console.log('mensaje');
    // this.router.navigate( ['/menu'] );
    // this.router.navigate(['inicio']);
  }

}
