import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServicioService {

  constructor(public http: HttpClient) { }

  url  = 'https://www.googleapis.com/auth/drive';
  getServ(): Observable<any> {
  return this.http.get<any>(this.url);
}

}
